@extends('layouts.app')

@section('content')
<div class="container">
  @if(isset($alert))
    <div class="alert alert-danger">
        <ul>
            <li>{{ $alert }}</li>
        </ul>
    </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
    </div>
  </div>
    <div class="row">
        <div class="col-xs-12">
          <div class="well">En este formulario puede administrar la información de las categorias en él o los centros que gestiona, de tal forma que se pueden incluir categorias, modificar o eliminar. Las categorías incluidas aquí se les puede asociar a los centros en el modulo de centros. </div>  
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Agregar categoría</div>

                <div class="panel-body">

                     @include('partials.addCategory')
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">Todas las categorías</div>

                <div class="panel-body">

                     @include('partials.tableCategory')
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.modalsCategory')
@endsection
