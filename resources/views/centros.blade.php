@extends('layouts.app')

@section('content')
<div class="container">
  @if(isset($alert))
    <div class="alert alert-danger">
        <ul>
            <li>{{ $alert }}</li>
        </ul>
    </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
    </div>
  </div>
    <div class="row">
      <div class="col-xs-12">
          <div class="well">En este modulo puede administrar la información de él o los centros que gestiona, de tal forma que se puede incluir, modificar o eliminar toda la información referente a un centro. Las categorías incluidas en el modulo de categorías se les puede asociar al centro que se quiera. Tales cambios realizados se veran reflejados en aplicacion móvil </div>  
        </div>
        <div class="col-xs-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Todas los centros / 
                  <a class="" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Agregar nuevo centro
                  </a>
                </div>

                <div class="panel-body">
                  @include('partials.addCentro')

                     @include('partials.tableCentro')
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.modalsCentro')
@endsection
