<!DOCTYPE HTML>
<html lang="{{ config('app.locale') }}">
<head>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Walk & Ride: Free Responsive HTML 5 Template</title>
    <meta name="description" content="A free responsive HTML 5 template with a clean style.">
    <meta name="keywords" content="free template, html 5, responsive, clean, scss">
    <link rel="apple-touch-icon" href="cic/images/touch/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="cic/images/touch/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="cic/images/touch/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="cic/images/touch/apple-touch-icon-144x144.png">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <link rel="stylesheet" href="cic/css/style.css">
    <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <script src="js/respond.js"></script>
    <![endif]-->
</head>
<body class="no-js">
    <div class="main">
        <header>
            <div class="wrap">
                <img src="cic/upload/iphone.png" height="532" width="252" alt="" class="header-img">
                <div class="header-wrapper">
                    <h1>Administración <span>Coupling Information Center</span></h1>
                    <p>
                        Sistema de administración para centros de acopio utiliza la aplicación móvil <strong>C.I.C.</strong> a través de la cual los adminstradores pueden gestionar la información de los centros, permitiendo ingresar uno o varios centros con sus caracteristicas y administrar la información que se va a presentar en la aplicación móvil.
                    </p>
                    <div class="buttons-wrapper">
                        <a href="{{ url('/register') }}" class="button">Registrarse</a>
                        <a href="{{ url('/login') }}" class="button button-stripe">Ingresar</a>
                    </div>
                </div>
                <!-- /.header-wrapper -->
            </div>
            <!-- /.wrap -->
        </header>

        <br></br>
           <!-- /.videos clearfix -->
            <div class="comments clearfix">
                <div class="wrap">
                    <div class="tab">
                        <div class="box visible">
                            <h4>Ventajas de Usar la Pagina</h4>
                            <p>El aministrador del centro puede modificar la información que esta en la aplicación móvil en tiempo real.</p>
                        </div>
                        <div class="box">
                            <h4>Beneficios</h4>
                            <p>El medio ambiente se ve beneficiado ya que se puede dar una disminución de la contaminación producto de la basura</p>
                        </div>
                        <div class="box">
                            <h4>Ventajas ambientales</h4>
                            <p>Se ahorra energía. 
                                Se reducen los costos de recolección. 
                                Se reduce el volumen de los residuos sólidos. 
                                Se conserva el ambiente y se reduce la contaminación.
                                Se alarga la vida útil de los sistemas de relleno sanitario. 
                                Hay remuneración económica en la venta de reciclables.</p>
                        </div>
                        <ul class="tabs">
                            <li class="active"></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="#" class="tab-prev"></a>
                        <a href="#" class="tab-next"></a>
                    </div>
                </div>
                <!-- /.wrap -->
            </div>

        <div class="spanning" id="spanning_id">
         <!-- /.promo clearfix -->
            <div class="discover clearfix">
                <div class="wrap">
                    <div class="discover-content clearfix">
                        <h2>CIC</h2>
                        <p>La Aplicación móvil CIC permite encontrar los centros de recolección en la comunidad de Puntarenas, mostrando las caracterisicas de dichos centros como la ubicación, horario, y demás información </p>
                        <div class="discover-button clearfix">
                            <!--<a href="#" class="button button-download">
                                <span class="button-download-title">Download for</span>
                                <span class="button-download-subtitle">Apple iOS</span>
                            </a>-->
                            <a href="#" class="button button-download android">
                                <span class="button-download-title">Descargar para</span>
                                <span class="button-download-subtitle">Android</span>
                            </a>
                        </div>
                    </div>
                    <div class="discover-img">
                        <div class="discover-img-inside"><img src="cic/upload/discover.png" height="486" width="634" alt=""></div>
                    </div>
                </div>
                <!-- /.wrap -->
            </div>
            <!-- /.discover clearfix -->
		 
           
        </div>
        <!-- /.spanning-columns -->
    </div>
    <!-- /.main -->
    <footer>
        <div class="wrap">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a> |
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif
            <p>&copy; 2017 <strong>C.I.C.</strong>, All Rights Reserved</p>
        </div>
        <!-- /.wrap -->
    </footer>
    <script src="cic/js/jquery.js"></script>
    <script src="cic/js/library.js"></script>
    <script src="cic/js/script.js"></script>
    <!-- <script src="cic/js/retina.js"></script> -->
</body>
</html>
